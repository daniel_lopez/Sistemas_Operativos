Daniel López Hernández

https://gitlab.com/SistemasOperativos-Ciencias-UNAM/sistemasoperativos-ciencias-unam.gitlab.io/blob/master/tareas/tarea-arch.md

Componentes del equipo:

-CPU: producto: Intel(R) Celeron(R) CPU  N3060  @ 1.60GHz
          fabricante: Intel Corp.
          id físico: 1
          información del bus: cpu@0
          tamaño: 1175MHz
          capacidad: 2480MHz
          anchura: 64 bits
         

-Memoria RAM:
          id físico: 0
          tamaño: 3850MiB

-Disco duro  descripción: ATA Disk
             producto: WDC WD5000LPCX-6
             fabricante: Western Digital
             id físico: 0.0.0
             información del bus: scsi@0:0.0.0
             nombre lógico: /dev/sda
             versión: 1A01
             serie: WD-WXJ1AC7P7VE9
             tamaño: 465GiB (500GB)
           
-Tarjetas de red 

Ethernet:
                descripción: Ethernet interface
                producto: RTL8111/8168/8411 PCI Express Gigabit Ethernet Controller
                fabricante: Realtek Semiconductor Co., Ltd.
                id físico: 0
                información del bus: pci@0000:01:00.0
                nombre lógico: eno1
                versión: 15
                serie: 10:e7:c6:ee:62:b2
                tamaño: 100Mbit/s
                capacidad: 1Gbit/s
                anchura: 64 bits
                reloj: 33MHz
          

Wireless:
                descripción: Interfaz inalámbrica
                producto: RTL8188EE Wireless Network Adapter
                fabricante: Realtek Semiconductor Co., Ltd.
                id físico: 0
                información del bus: pci@0000:02:00.0
                nombre lógico: wlo1
                versión: 01
                serie: 5c:ea:1d:2c:54:02
                anchura: 64 bits
                reloj: 33MHz
         

-Dispositivos:

-display
             descripción: VGA compatible controller
             producto: Intel Corporation
             fabricante: Intel Corporation
             id físico: 2
             información del bus: pci@0000:00:02.0
             versión: 35
             anchura: 64 bits
             reloj: 33MHz
             
-usb
             descripción: USB controller
             producto: Intel Corporation
             fabricante: Intel Corporation
             id físico: 14
             información del bus: pci@0000:00:14.0
             versión: 35
             anchura: 64 bits
             reloj: 33MHz
             
-multimedia
             descripción: Audio device
             producto: Intel Corporation
             fabricante: Intel Corporation
             id físico: 1b
             información del bus: pci@0000:00:1b.0
             versión: 35
             anchura: 64 bits
             reloj: 33MHz
          

